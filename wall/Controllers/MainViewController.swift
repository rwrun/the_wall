//
//  MainViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 01.09.2020.
//

import UIKit

class MainViewController: UIViewController {
    
    var controller: UIViewController!
    var menuViewController: UIViewController!
    var cameraViewController: CameraViewController!
    var addPostViewController: AddPostViewController!
    var detailViewController: DetailsViewController!
    var isMove = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addContentView(liked: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .darkContent
    }
    private func addContentView(liked: Int?){
        let contentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ContentViewController") as! ContentViewController
        contentVC.delegate = self
        contentVC.liked = liked
        controller = contentVC
        view.addSubview(controller.view)
    }
    
    private func addMenuView(){
        if menuViewController == nil {
            let menuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MenuViewController") as! MenuViewController
            menuVC.delegate = self
            menuViewController = menuVC
            view.insertSubview(menuViewController.view, at: 0)
            addChild(menuViewController)
            print("Добавили меню")
        }
    }
    private func addCameraView(){
        if cameraViewController == nil{
            cameraViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CameraViewController")
            cameraViewController.delegate = self
            view.insertSubview(cameraViewController.view, at: 1)
            addChild(cameraViewController)
            print("Добавили камеру")
        }
    }
    
    private func addPostView(image: UIImage){
        if addPostViewController == nil{
            addPostViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "AddPostViewController")
            addPostViewController.delegate = self
            addPostViewController.image = image
            view.insertSubview(addPostViewController.view, at: 2)
            addChild(addPostViewController)
            print("add PostVC")
        }
    }
    private func showDetails(image: String, text: String, itemId: String){
        if detailViewController == nil {
            detailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "DetailsViewController")
            detailViewController.image = image
            detailViewController.descriptionText = text
            detailViewController.itemId = itemId
            detailViewController.delegate = self
            view.insertSubview(detailViewController.view, at: 3)
            addChild(detailViewController)
        }
    }
    
    func showCameraViewController(shouldMove: Bool){
        if shouldMove{
            UIView.animate(withDuration: 0.2,
                           delay: 0,
                           options: .curveEaseOut) {
                self.controller.view.frame.origin.x = self.controller.view.frame.width
            } completion: { (finished) in
                
            }
        } else {
            
            UIView.animate(withDuration: 0.2,
                           delay: 0,
                           options: .curveEaseOut) {
                self.controller.view.frame.origin.x = 0
            } completion: { (finished) in
                
            }
        }
    }
    func showMenuViewController(shouldMove: Bool){
        if shouldMove {
            self.view.isMultipleTouchEnabled = false
            UIView.animate(withDuration: 0.2,
                           delay: 0,
                           options: .curveEaseOut) {
                self.controller.view.frame.origin.x = self.controller.view.frame.width - 150
            } completion: { (finished) in
                
            }
        } else {
            self.view.isMultipleTouchEnabled = true
            UIView.animate(withDuration: 0.2,
                           delay: 0,
                           options: .curveEaseOut) {
                self.controller.view.frame.origin.x = 0
            } completion: { (finished) in
                
            }
        }
    }
}

// MARK:-- ContentViewControllerDelegate

extension MainViewController: ContentViewControllerDelegate{
    
    func toggleState(type: State) {
        //isMove = !isMove
        switch type {
        case .Add:
            addCameraView()
            showCameraViewController(shouldMove: true)
        case .CameraBack:
            showCameraViewController(shouldMove: false)
            let deadlineTime = DispatchTime.now() + .milliseconds(200)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                self?.cameraViewController.view.removeFromSuperview()
                self?.cameraViewController = nil
            }
        case .RePhoto:
            addPostViewController.view.removeFromSuperview()
            addPostViewController = nil
            
            cameraViewController.view.removeFromSuperview()
            cameraViewController = nil
            
            addCameraView()
        case .Close:
            showCameraViewController(shouldMove: false)
            let deadlineTime = DispatchTime.now() + .milliseconds(400)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                self?.cameraViewController.view.removeFromSuperview()
                self?.cameraViewController = nil
                
                self?.addPostViewController.view.removeFromSuperview()
                self?.addPostViewController = nil
            }
        case .Liked:
            isMove = !isMove
            likeIsShow(isShow: isMove, liked: 1)
        case .All:
            isMove = !isMove
            likeIsShow(isShow: isMove, liked: nil)
        default:
            addMenuView()
            isMove = !isMove
            showMenuViewController(shouldMove: isMove)
        }
    }
    func likeIsShow(isShow: Bool, liked: Int?){
        showMenuViewController(shouldMove: isShow)
        controller.view.removeFromSuperview()
        controller = nil
        addContentView(liked: liked)
    }
    func addPostState(image: UIImage) {
        print("show add post VC")
        addPostView(image: image)
    }
    
    func showDetailsPage(image: String, text: String, itemId: String) {
        showDetails(image: image, text: text, itemId: itemId)
    }
    
    func closeDetails() {
        detailViewController.view.removeFromSuperview()
        detailViewController = nil
    }
}

