//
//  MenuViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 01.09.2020.
//

import UIKit

class MenuViewController: UIViewController {

    weak var delegate: ContentViewControllerDelegate?
    var item: Int?
    var isLiked = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func settingBtn(_ sender: Any) {
        let settingCollection = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SettingsViewController")
        present(settingCollection, animated: true, completion: nil)
    }
    
    @IBAction func showLikeItems(_ sender: UIButton) {
        isLiked = !isLiked
        if isLiked{
            sender.setTitle("Все", for: .normal)
            delegate?.toggleState(type: .Liked)
        } else {
            sender.setTitle("Избранное", for: .normal)
            delegate?.toggleState(type: .All)
        }
    }
    
}
