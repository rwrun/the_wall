//
//  ContentViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 01.09.2020.
//

import UIKit
import RealmSwift


class ContentViewController: UIViewController {

    @IBOutlet weak var addPostBtn: UIButton!
    
    var collectionView: UICollectionView!
    var dataSource: UICollectionViewDiffableDataSource<PSections, PItems>?
    var cellBounds: CGRect?
    var liked: Int?
    var postItems:[PItems] = []
    var likeState: Int?
    weak var delegate: ContentViewControllerDelegate?
    
    let reControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refreshData(sendler:)), for: .valueChanged)
        return refresh
    }()
    
    var posts = try! Realm().objects(PSections.self).toArray(ofType: PSections.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //liked = likeState
        
        cellBounds = CGRect(x: 0, y: 0, width: 0, height: 0)
        addPostBtn.layer.cornerRadius = addPostBtn.bounds.width/2
        setupCollectionView()
        createDataSource()
        reloadData()
        view.insertSubview(addPostBtn, at: 2)
    }
    private func setupCollectionView(){
        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: createCompositionLayout())
        
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        collectionView.backgroundColor = #colorLiteral(red: 0.9450049996, green: 0.9451631904, blue: 0.9449841976, alpha: 1)
        
        collectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemCollectionViewCell")
        
        collectionView.delegate = self
        collectionView.refreshControl = reControl
        view.addSubview(collectionView)
        view.insertSubview(collectionView, at: 1)
    }
    
    @objc func refreshData(sendler: UIRefreshControl){
        createDataSource()
        reloadData()
        sendler.endRefreshing()
    }
    
    private func createDataSource(){
        dataSource = UICollectionViewDiffableDataSource<PSections, PItems>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
            cell.config(with: item)
            return cell
            
        })
    }
    
    private func reloadData(){
        var snapshot = NSDiffableDataSourceSnapshot<PSections, PItems>()
        snapshot.appendSections(posts)
        
        for post in posts {
            if liked != nil, liked == 1{
                postItems = post.items.sorted(byKeyPath: "id", ascending: false).filter("favorites = 1").toArray(ofType: PItems.self)
            } else {
                postItems = post.items.sorted(byKeyPath: "id", ascending: false).toArray(ofType: PItems.self)
            }
            snapshot.appendItems(postItems, toSection: post)
        }
        dataSource?.apply(snapshot)
    }
    
    private func createCompositionLayout() -> UICollectionViewLayout{
        let layout = UICollectionViewCompositionalLayout { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
            let sections = self.posts[sectionIndex].id
            switch sections{
            default:
                return self.createPostSection()
            }
        }
        return layout
    }
    
    private func createPostSection() -> NSCollectionLayoutSection{
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        
        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20)
        
        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(357))
        let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])
        
        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        layoutSection.contentInsets = NSDirectionalEdgeInsets(top: 80, leading: 0, bottom: 0, trailing: 0)
        //layoutSection.orthogonalScrollingBehavior = .groupPagingCentered
        
        return layoutSection
    }
    
    
    @IBAction func presentCameraVC(_ sender: Any) {
        delegate?.toggleState(type: .Add)
    }
    
    @IBAction func showMenuBtn(_ sender: Any) {
        delegate?.toggleState(type: .Menu)
    }
    
    @IBAction func screenEdgeGestureLeft(_ sender: UIScreenEdgePanGestureRecognizer) {
        let translate = sender.translation(in: sender.view)
        let state = sender.state
        if translate.x > 80, state == .ended{
            delegate?.toggleState(type: .Menu)
        }
        //delegate?.toggleState(type: .Menu)
    }
}

extension ContentViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "DetailsViewController")
        
        navigationController?.pushViewController(detailsVC, animated: true)
        
        let cell = collectionView.cellForItem(at: indexPath) as? ItemCollectionViewCell
        
        
        let image = postItems[indexPath.row].image
        let descriptionText = postItems[indexPath.row].descriptionText
        let itemId = postItems[indexPath.row].id
        self.collectionView?.bringSubviewToFront(cell!)
        cellBounds = cell?.frame

        UIView.animate(withDuration: 0.2) {
            cell?.frame = collectionView.bounds
            cell?.itemContent.frame = cell!.bounds
            cell?.itemContentBar.alpha = 0
        } completion: { (finished) in
            if finished {
                self.delegate?.showDetailsPage(image: image, text: descriptionText, itemId: itemId)
                cell?.frame = self.cellBounds!
                cell?.itemContentBar.alpha = 1
                cell?.itemContent.frame = self.cellBounds!
            }
        }
    }
}
