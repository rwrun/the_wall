//
//  CameraViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 03.09.2020.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController {

    var imagePicker: UIImagePickerController!
    var captureSession = AVCaptureSession()
    
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var curentDevice: AVCaptureDevice?
    
    var photoOutput: AVCapturePhotoOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var photoResult: UIImage?
    weak var delegate: ContentViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupCaptureSession()
        setupDevice(position: 1)
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
        
    }
    func setupCaptureSession(){
        captureSession.sessionPreset = .photo
    }
    
    
    func setupDevice(position: Int){
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        let devices = deviceDiscoverySession.devices
        
        for device in devices{
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            } else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        
        if position == 1{
            curentDevice = backCamera
        } else {
            curentDevice = frontCamera
        }
        
    }
    
    func setupInputOutput(){
        do {
            if curentDevice != nil{
                let captureDeviceInput = try AVCaptureDeviceInput(device: curentDevice!)
                captureSession.addInput(captureDeviceInput)
                photoOutput = AVCapturePhotoOutput()
                photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
                captureSession.addOutput(photoOutput!)
            }
        } catch {
            print("error")
        }
    }
    
    func setupPreviewLayer(){
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = .resizeAspectFill
        previewLayer?.connection?.videoOrientation = .portrait
        previewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(previewLayer!, at: 0)
        
    }
    
    func startRunningCaptureSession(){
        captureSession.startRunning()
    }
    
    
    @IBAction func closeCamera(_ sender: Any) {
        delegate?.toggleState(type: .CameraBack)
        captureSession.stopRunning()
    }
    
    
    @IBAction func takeAPhoto(_ sender: Any) {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: self)
        
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard segue.identifier == "showImageVC" else { return }
//        guard let imageVC = segue.destination as? ShowImageViewController else { return }
//        imageVC.shotPhoto = photoResult
//
//    }
    
    @IBAction func toggleCameraPosition(_ sender: Any) {
        
        captureSession.beginConfiguration()
            
        // Change the device based on the current camera
        let newDevice = (curentDevice?.position == AVCaptureDevice.Position.back) ? frontCamera : backCamera
        // Remove all inputs from the session
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureDeviceInput)
        }
        // Change to the new input
        let cameraInput:AVCaptureDeviceInput
        do {
            cameraInput = try AVCaptureDeviceInput(device: newDevice!)
        } catch {
            print(error)
            return
        }
        if captureSession.canAddInput(cameraInput) {
            captureSession.addInput(cameraInput)
        }
        curentDevice = newDevice
        captureSession.commitConfiguration()
    }
    
    @IBAction func selectPhotoInAlbum(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
}
extension CameraViewController: AVCapturePhotoCaptureDelegate{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let photoData = photo.fileDataRepresentation(){
            print(photoData)
            photoResult = UIImage(data: photoData)
            if let image = photoResult{
                delegate?.addPostState(image: image)
            } else {
                print("Not found")
            }
            captureSession.stopRunning()
        }
    }
}
extension CameraViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        captureSession.stopRunning()
        
        let libraryPhoto = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        dismiss(animated: true, completion: nil)
        if let image = libraryPhoto{
            delegate?.addPostState(image: image)
        } else {
            print("Not found")
        }
        
    }
}
