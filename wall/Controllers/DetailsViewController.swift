//
//  DetailsViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 08.09.2020.
//

import UIKit
import SDWebImage
import FloatingPanel


class DetailsViewController: UIViewController, FloatingPanelControllerDelegate {

    @IBOutlet weak var detailBgImage: UIImageView!
    var image: String?
    var descriptionText: String?
    var itemId: String?
    var delegate: ContentViewControllerDelegate?
    var tap: UITapGestureRecognizer!
    var leftGesture: UIScreenEdgePanGestureRecognizer!
    let alert = Alert()
    let reviewsService = ReviewsService()
    let postService = PostService()
    var openImage: UIPinchGestureRecognizer!
    var imageScrollView: ImageScrollView!
    var imageView: UIImage?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var noteTextViewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFloatingPanel()
        
        let imageUrl = filePath(forKey: image!)
        detailBgImage.sd_setImage(with: imageUrl, completed: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard))
        view.addGestureRecognizer(tap)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapF))
        
        doubleTap.numberOfTapsRequired = 2
        detailBgImage.isUserInteractionEnabled = true
        detailBgImage.addGestureRecognizer(doubleTap)
        
        noteTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        leftGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(closeDetails(_:)))
        leftGesture.edges = .left
        view.addGestureRecognizer(leftGesture)
        
//        openImage = UIPinchGestureRecognizer(target: self, action: #selector(openImageAction(_:)))
//        detailBgImage.isUserInteractionEnabled = true
//        detailBgImage.addGestureRecognizer(openImage)
    }
    
    
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        
        return FloatingPanelStocksLayout()
    }
    
    func setFloatingPanel(){
        let fpc = FloatingPanelController()
        fpc.delegate = self
        
        guard let notesVC = storyboard?.instantiateViewController(identifier: "fpc_notes") as? NotesViewController else { return }
        notesVC.descriptionText = descriptionText
        
        notesVC.itemId = itemId
        
        fpc.set(contentViewController: notesVC)
        fpc.addPanel(toParent: self)
        
        fpc.surfaceView.backgroundColor = UIColor(displayP3Red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 0.0)
        fpc.surfaceView.cornerRadius = 10
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.borderWidth = 1.0 / traitCollection.displayScale
        fpc.surfaceView.borderColor = UIColor.black.withAlphaComponent(0.2)
        
    }
        
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        return documentURL.appendingPathComponent(key + ".jpeg")
    }

    @IBAction func closeBtn(_ sender: Any) {
        delegate?.closeDetails()
    }
    
    //MARK:-- Actions
    @objc func closeDetails(_ sender: UIScreenEdgePanGestureRecognizer){
        let translate = sender.translation(in: sender.view)
        if translate.x > 80{
            delegate?.closeDetails()
        }
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            UIView.animate(withDuration: 0.3) {
                self.heightConstraint.constant = keyboardHeight
                self.view.layoutIfNeeded()
            }
        }
    }
    @objc private func dissmissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func addNoteAcrion(_ sender: Any) {
        let text = noteTextField.text
        if let text = text{
            if text.count > 0{
                noteTextField.text = ""
                reviewsService.addReviewToItem(itemId: itemId!, comment: text, completion: { [weak self] in
                    let successAlert = alert.alertAction(title: "Готово", message: "Заметка добавлена")
                    self?.present(successAlert, animated: true, completion: nil)
                })
            } else{
                let falseAlert = alert.alertAction(title: "Ошибка", message: "Текстовое поле не может быть пустым")
                self.present(falseAlert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func showMoreMenu(_ sender: Any) {
        let menu = moreMenu()
        self.present(menu, animated: true, completion: nil)
        
    }
    
    func moreMenu() -> UIAlertController{
        let menuAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let delletePostAlert = UIAlertAction(title: "Удалить", style: .destructive) { (action) in
            
            self.postService.deletePost(postId: self.itemId!) {
                self.delegate?.closeDetails()
            }
        }
        let sharePostImage = UIAlertAction(title: "Поделиться", style: .default) { (action) in
            let imageShareData = self.filePath(forKey: self.image!)
            let data = try? Data(contentsOf: imageShareData!)
            let imageSharing = UIImage(data: data!)
            let shareData = [self.descriptionText ?? "" , imageSharing!] as [Any]
            let shareActivity = UIActivityViewController(activityItems: shareData, applicationActivities: nil)
            
            self.present(shareActivity, animated: true, completion: nil)
        }
        
        let cancelBtnAcrion = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        menuAlert.addAction(delletePostAlert)
        menuAlert.addAction(sharePostImage)
        menuAlert.addAction(cancelBtnAcrion)
        
        return menuAlert
    }
    
    @objc func doubleTapF(){
        
        print("double tap")
        
        imageScrollView = ImageScrollView(frame: view.bounds)
        imageScrollView.backgroundColor = .black
        view.addSubview(imageScrollView)
        setupImageScrollView()
        
        let imageUrl = self.filePath(forKey: self.image!)
        let data = try! Data(contentsOf: imageUrl!)
        if let image = UIImage(data: data) {
            self.imageView = image
        }
        self.imageScrollView.closeDelegare = self
        self.imageScrollView.set(image: self.imageView!)
    }
    
    func setupImageScrollView() {
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        imageScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        imageScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    }
    
}

extension DetailsViewController: UITextFieldDelegate, CloseImageViewDelegate{

    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            self.heightConstraint.constant = 60
            self.view.layoutIfNeeded()
        }
    }
   
    func closeImageView() {
        imageScrollView.removeFromSuperview()
        imageScrollView = nil
    }
}
