//
//  NotesViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 19.09.2020.
//

import UIKit
import RealmSwift

class NotesViewController: UIViewController {

    @IBOutlet weak var postDescriptionText: UILabel!
    var descriptionText: String?
    var itemId: String?
    let realm = try! Realm()
    var itemsForId: [PItems] = []
    @IBOutlet weak var notesTable: UITableView!
    
    var commets: [PReviews]?
    
    let reControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refreshNotes(sendler:)), for: .valueChanged)
        return refresh
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postDescriptionText.text = descriptionText
        notesTable.delegate = self
        notesTable.dataSource = self
        notesTable.refreshControl = reControl
        let noteTableCell = UINib(nibName: "NotesItemTableViewCell", bundle: nil)
        notesTable.backgroundColor = .none
        notesTable.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        notesTable.register(noteTableCell, forCellReuseIdentifier: "NotesItemTableViewCell")
        getRevForId(id: itemId!)
    }
    
    func getRevForId(id: String){
        itemsForId = realm.objects(PItems.self).filter("id = '\(id)'").toArray(ofType: PItems.self)
        commets = itemsForId.first?.comments.toArray(ofType: PReviews.self)
    }
    
    @objc func refreshNotes(sendler: UIRefreshControl){
        getRevForId(id: itemId!)
       
        self.notesTable.reloadData()
        sendler.endRefreshing()
    }
}
extension NotesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commets?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesItemTableViewCell") as! NotesItemTableViewCell
        
        let text = commets?[indexPath.row].content
        let date = commets?[indexPath.row].date
        let viewSeparatorLine = UIView(frame:CGRect(x: 0, y: cell.contentView.frame.size.height - 8.0, width: cell.contentView.frame.size.width, height: 8))
        
        cell.contentView.addSubview(viewSeparatorLine)
        
        if let text = text{
            cell.configCell(text: text, date: date!)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delBtn = UIContextualAction(style: .normal, title: "Удалить") { (action, view, completion) in
            let commentId = self.commets?[indexPath.row].commentId
            let comment = self.realm.objects(PReviews.self).filter("commentId = '\(commentId!)'").first
            do {
                try self.realm.write{
                    self.realm.delete(comment!)
                    self.getRevForId(id: self.itemId!)
                    self.notesTable.reloadData()
                }
            } catch{
                
            }
            completion(true)
        }
        delBtn.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0)
        let swipeAction = UISwipeActionsConfiguration(actions: [delBtn])
        return swipeAction
    }
}
