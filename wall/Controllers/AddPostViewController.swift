//
//  AddPostViewController.swift
//  wall
//
//  Created by Ислам Батыргереев on 05.09.2020.
//

import UIKit
import Lottie

class AddPostViewController: UIViewController {
    
    @IBOutlet weak var addTextContainer: UIVisualEffectView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var textContent: UITextView!

    @IBOutlet weak var contentView: UIView!
    let postService = PostService()
    let alert = Alert()
    var isMove = false
    var delegate: ContentViewControllerDelegate?
    var image: UIImage?
    var tap: UITapGestureRecognizer!
    
    @IBOutlet weak var completeContent: UIVisualEffectView!
    @IBOutlet weak var completeAnimationContainer: UIView!
    var animationView: AnimationView?
    @IBOutlet weak var suceesBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textContent.delegate = self
        tap = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard))
        view.addGestureRecognizer(tap)
        
        postImage.image = image
        addTextContainer.layer.cornerRadius = 10
        customNavBar.layer.cornerRadius = 20
        
        animationView = .init(name: "check")
        animationView?.frame = completeAnimationContainer.bounds
        completeAnimationContainer.addSubview(animationView!)

    }

    @IBAction func closeAddBtn(_ sender: UIButton) {
        delegate?.toggleState(type: .Close)
    }
    
    @IBAction func rePhotoBtn(_ sender: UIButton) {
        delegate?.toggleState(type: .RePhoto)
    }
    
   @objc func dissmissKeyboard(){
        if isMove{
            isMove = !isMove
            moveView(isMove: isMove)
            view.endEditing(true)
        }
        
    }
    
    func moveView(isMove: Bool){
        if isMove{
            UIView.animate(withDuration: 0.3, delay: 0.3, options: .allowAnimatedContent, animations: {
                self.contentView.frame.origin.y = -100
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3) {
                self.contentView.frame.origin.y = 0
            }
        }
    }
    
    @IBAction func postAddBtn(_ sender: Any) {
        if let text = textContent.text{
            if text.count < 140{
                showCompleteContent()
                postService.setData(text: text, image: image!, completion: { [weak self] in
                    DispatchQueue.main.async {
                        //self?.delegate?.toggleState(type: .Close)
                        self?.suceesBtn.isHidden = false
                    }
                })
            } else {
                let alertActionController = alert.alertAction(title: "Внимание", message: "Текстовое поле не должно содержать больше 140 символов.")
                self.present(alertActionController, animated: true, completion: nil)
            }
        } else {
            showCompleteContent()
            postService.setData(text: textContent.text, image: image!, completion: { [weak self] in
                DispatchQueue.main.async {
                    self?.suceesBtn.isHidden = false
                }
            })
        }
    }
    
    @IBAction func successBtn(_ sender: Any) {
        delegate?.toggleState(type: .Close)
    }
    
    func showCompleteContent(){
        animationView?.play()
        completeContent.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.completeContent.alpha = 1
        }
    }
}

extension AddPostViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        isMove = !isMove
        moveView(isMove: isMove)
    }
}

