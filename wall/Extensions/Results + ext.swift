//
//  Results + ext.swift
//  myLibrary
//
//  Created by Ислам Батыргереев on 15.08.2020.
//

import Foundation
import RealmSwift

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        let array = Array(self) as! [T]
        return array
    }
}
