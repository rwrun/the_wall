//
//  PostService.swift
//  wall
//
//  Created by Ислам Батыргереев on 06.09.2020.
//

import UIKit
import RealmSwift

class PostService{
    
    var imageNewName: String?
    let realm = try! Realm()
    
    let secondsIntrval = Date.timeIntervalSinceReferenceDate
    let postSetion = try! Realm().objects(PSections.self).filter("id = 1")
    
    func setData(text: String?, image: UIImage, completion: @escaping ()->()){
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let nowDate = formatter.string(from: date)
        
        imageNewName = UUID().uuidString
        DispatchQueue.global(qos: .background).async{
            self.storeImage(image: image, forKey: self.imageNewName!)
            completion()
        }
        addPost(section: postSetion, text: text, date: nowDate, image: imageNewName!)
    }
    
    private func addPost(section: Results<PSections>, text: String?, date: String, image: String){
        let postItem = PItems()
        let postItemId = Int(secondsIntrval)
        
        postItem.id = "\(postItemId)"
        postItem.descriptionText = text ?? ""
        postItem.favorites = 0
        postItem.image = image
        
        if let firstSection = section.first{
            do {
                try realm.write{
                    firstSection.items.append(postItem)
                    print("data add")
                }
            } catch {
                print(error)
            }
        }
    }
    
    func deletePost(postId: String, completion: ()->()){
        let postForId = realm.objects(PItems.self).filter("id = '\(postId)'").first
        let comments = postForId?.comments.toArray(ofType: PReviews.self)
        let fileName = postForId?.image
        let fileManager = FileManager.default
        
        do {
            try realm.write{
                realm.delete(comments!)
                realm.delete(postForId!)
                
                try! fileManager.removeItem(at: filePath(forKey: fileName!)!)
                
                completion()
            }
        } catch {
            print("error")
        }
    }
    
    func likePost(completion: (Int?)->(), postId: String){
        var liked: Int?
        
        let postForId = realm.objects(PItems.self).filter("id = '\(postId)'").first
        let like = postForId?.favorites
        
        switch like {
        case 0:
            liked = 1
        default:
            liked = 0
        }
        
        do {
            try realm.write{
                postForId?.favorites = liked!
                completion(liked)
            }
        } catch {
            print(error)
        }
    }
    
    private func storeImage(image: UIImage, forKey key: String){
        if let jpgRepresentional = image.jpegData(compressionQuality: 0.7){
            if let filePath = filePath(forKey: key){
                do {
                    try jpgRepresentional.write(to: filePath, options: .atomic)
                } catch let err{
                    print("Error to saving: ", err)
                }
            }
        }
    }
    
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        return documentURL.appendingPathComponent(key + ".jpeg")
    }
}
