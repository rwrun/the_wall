//
//  ReviewsService.swift
//  wall
//
//  Created by Ислам Батыргереев on 24.09.2020.
//

import Foundation
import RealmSwift

class ReviewsService {
    let realm = try! Realm()
    
    
    func addReviewToItem(itemId: String, comment: String, completion: ()->()){
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let nowDate = formatter.string(from: date)
        
        let revId = Int(getInteval())
        
        let newComment = PReviews()
        newComment.commentId = "\(revId)"
        newComment.date = nowDate
        newComment.content = comment
        
        let item = realm.objects(PItems.self).filter("id = '\(itemId)'")
        
        do {
            try realm.write{
                item.first?.comments.append(newComment)
                completion()
            }
        } catch {
            print(error)
        }
    }
    
    func getInteval() -> TimeInterval{
        let secondsIntrval = Date.timeIntervalSinceReferenceDate
        return secondsIntrval
    }
    
    
}
