//
//  Alert.swift
//  wall
//
//  Created by Ислам Батыргереев on 06.09.2020.
//

import UIKit

class Alert {
    func alertAction(title: String, message: String)->UIAlertController{
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlert = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAlert)
        
        return alertController
    }
}
