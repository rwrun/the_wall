//
//  FloatingPanelStocksLayout.swift
//  wall
//
//  Created by Ислам Батыргереев on 21.09.2020.
//

import Foundation
import FloatingPanel

class FloatingPanelStocksLayout: FloatingPanelLayout {
    var initialPosition: FloatingPanelPosition {
        return .tip
    }

    var topInteractionBuffer: CGFloat { return 0.0 }
    var bottomInteractionBuffer: CGFloat { return 0.0 }
    
    func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .full: return 56.0
        case .half: return 262.0
        case .tip: return 10.0 + 44.0 // Visible + ToolView
        default: return nil
        }
    }

}
