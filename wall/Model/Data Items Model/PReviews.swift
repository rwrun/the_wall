//
//  PReviews.swift
//  wall
//
//  Created by Ислам Батыргереев on 01.09.2020.
//

import Foundation
import RealmSwift

class PReviews: Object{
    @objc dynamic var commentId = ""
    @objc dynamic var date = ""
    @objc dynamic var content = ""
    
    override class func primaryKey() -> String? {
        return "commentId"
    }
}
