//
//  PItems.swift
//  wall
//
//  Created by Ислам Батыргереев on 01.09.2020.
//

import Foundation
import RealmSwift

class PItems: Object {
    @objc dynamic var id = ""
    @objc dynamic var favorites = 0
    @objc dynamic var image = ""
    @objc dynamic var descriptionText = ""
    var comments = List<PReviews>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
