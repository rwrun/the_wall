//
//  ContentViewControllerDelegate.swift
//  wall
//
//  Created by Ислам Батыргереев on 03.09.2020.
//

import UIKit

protocol ContentViewControllerDelegate: class {
    func toggleState(type: State)
    func addPostState(image: UIImage)
    func showDetailsPage(image: String, text: String, itemId: String)
    
    func closeDetails()
}
