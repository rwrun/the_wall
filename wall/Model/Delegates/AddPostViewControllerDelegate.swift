//
//  AddPostViewControllerDelegate.swift
//  wall
//
//  Created by Ислам Батыргереев on 05.09.2020.
//

import UIKit

protocol AddPostViewControllerDelegate: class {
    func photoImage(image: UIImage?)
}
