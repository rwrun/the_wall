//
//  CloseImageViewDelegate.swift
//  wall
//
//  Created by Ислам Батыргереев on 02.10.2020.
//

import Foundation

protocol CloseImageViewDelegate: class {
    func closeImageView()
}
