//
//  NotesItemTableViewCell.swift
//  wall
//
//  Created by Ислам Батыргереев on 24.09.2020.
//

import UIKit

class NotesItemTableViewCell: UITableViewCell {
    @IBOutlet weak var noteContainer: UIView!
    @IBOutlet weak var noteText: UILabel!
    @IBOutlet weak var noteDate: UILabel!
    @IBOutlet weak var noteContent: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(text: String, date: String){
        noteContent.layer.cornerRadius = 10
        noteContent.clipsToBounds = true
        noteText.text = text
        noteDate.text = date
    }
    
}
