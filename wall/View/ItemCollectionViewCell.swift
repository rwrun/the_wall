//
//  ItemCollectionViewCell.swift
//  wall
//
//  Created by Ислам Батыргереев on 01.09.2020.
//

import UIKit
import SDWebImage

class ItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var itemContent: UIView!
    @IBOutlet weak var commentCount: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemContentBar: UIVisualEffectView!
    @IBOutlet weak var commetnCountText: UILabel!
    var curentDate = Date()
    let formatter = DateFormatter()
    var itemId: String?
    let postService = PostService()
    @IBOutlet weak var daysAgo: UILabel!
    
    @IBOutlet weak var likeItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
        
    func config(with item: PItems){
        let id = Int(item.id)
        self.itemId = item.id
        let curentDateInterval = Int(curentDate.timeIntervalSinceReferenceDate)
        let dateDifferences = Double(curentDateInterval - id!)
        let dateDifferencesDate = Int(round(dateDifferences/86400))
        
        itemContent.layer.cornerRadius = 7
        itemContent.clipsToBounds = true
        commentCount.layer.cornerRadius = commentCount.bounds.width/2
        switch dateDifferencesDate {
        case 0:
            daysAgo.text = "сегодня"
        case 1:
            daysAgo.text = "\(dateDifferencesDate) день назад"
        case 2...4:
            daysAgo.text = "\(dateDifferencesDate) дня назад"
        default:
            daysAgo.text = "\(dateDifferencesDate) дней назад"
        }
        let likeStatus = item.favorites
        
        switch likeStatus {
        case 0:
            likeItem.image = UIImage(named: "like_standart")
        default:
            likeItem.image = UIImage(named: "like")
        }
        
        
        let comments = item.comments.count
        commetnCountText.text = "\(comments)"
        if let imageName = filePath(forKey: item.image) {
            itemImage.sd_setImage(with: imageName, completed: nil)
        }
        
        likeItem.isUserInteractionEnabled = true
        
        let tapLike = UITapGestureRecognizer(target: self, action: #selector(tapToLike))
        
        likeItem.addGestureRecognizer(tapLike)
        
        
    }
    
    @objc func tapToLike(){
        self.postService.likePost(completion: { (liked) in
            switch liked {
            case 0:
                likeItem.image = UIImage(named: "like_standart")
            default:
                likeItem.image = UIImage(named: "like")
            }
        }, postId: itemId!)
        
    }
    
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        return documentURL.appendingPathComponent(key + ".jpeg")
    }
}
